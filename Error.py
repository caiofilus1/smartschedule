from enum import Enum
from flask import jsonify


class Error(Enum):
    NO_ERROR = 0

    ERROR_NOT_A_JSON = 1
    ERROR_MISSING_VALUE = 2
    ERROR_MISSING_TOKEM = 3
    ERROR_INVALID_TOKEN = 4

    ERROR_ON_DB = 10

    ERROR_USER_NOT_FOUND = 20
    ERROR_ALREADY_EXIST = 21
    ERROR_USER_OR_PASS_INCORRECT = 22
    ERROR_USER_NOT_LOGGED = 23

    ERROR_ROUTE_ALREADY_EXIST = 40
    ERROR_DATE_FORMAT_INCORRECT = 41
    ERROR_ROUTE_ALREADY_SENDED = 42
    ERROR_ROUTE_NOT_FULL_SENDED = 43

    @staticmethod
    def get_json_status(error):
        return jsonify({'status': error.value})
