from datetime import timedelta
from functools import wraps

from flask import request, jsonify

from Error import Error
from Model.Device import Device


def require_api_key(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        if request.is_json:
            json = request.get_json()
            api_key = ''
            try:
                api_key = json['api_key']
            except KeyError:
                return jsonify({'status': Error.ERROR_MISSING_VALUE.value})

            device = Device.query.filter_by(api_key=api_key).first()
            '''for device_logged in devices_logged:
                if device_logged['device'].id == device.id:
                    if device_logged['ip'] != request.remote_addr:
                        device_logged['ip'] = request.remote_addr
                        break

            devices_logged.append({'device': device, 'ip': request.remote_addr})        
'''''
            if device is not None:
                global current_device
                current_device = device
                return f(*args, **kwargs)
            else:
                return jsonify({'status': Error.ERROR_INVALID_TOKEN.value})
        else:
            return jsonify({'status': Error.ERROR_NOT_A_JSON.value})

    return decorator

def last_day(d, day_name):
    days_of_week = ['sunday','monday','tuesday','wednesday',
                        'thursday','friday','saturday']
    target_day = days_of_week.index(day_name.lower())
    delta_day = target_day - d.isoweekday()
    if delta_day >= 0: delta_day -= 7 # go back 7 days
    return d + timedelta(days=delta_day)
