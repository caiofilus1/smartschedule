import os
from datetime import datetime

from flask import Blueprint, render_template, request, url_for, flash, jsonify
from flask_login import login_required, login_user
from werkzeug.utils import redirect

import database
from Forms.AdminLoginForm import AdminLoginForm
from Forms.DeviceForm import DeviceForm
from Model.Admin import Admin
from Model.Device import Device
from Model.Schedule import Schedule
from Model.User import User, Bond
from database import db
from templates.Tables.DevicesTable import DevicesTable
from templates.Tables.ScheduleTable import ScheduleTable
from templates.Tables.UserTable import UserTable
from templates.Views.ManualScheduleForm import ManualScheduleForm
from templates.Views.UserForm import UserForm

path = os.path.dirname(os.path.abspath(__file__))
admin = Blueprint('admin', __name__,
                  static_folder=path + '/static',
                  template_folder=path + '/templates',
                  url_prefix='/admin')


@admin.route('/users')
@login_required
def users_edit_page():
    users = User.query.order_by(User.name).all()
    user_table = UserTable(users)

    last_cards = database.last_cards

    # schedule = Schedule.query.all()
    # schedule_table = ScheduleTable(schedule)
    return render_template('admin.html',
                           user_form=UserForm(),
                           user_table=user_table,
                           last_cards=last_cards,
                           bond_values=Bond.list()
                           )


@admin.route('/devices')
@login_required
def devices_page():
    devices = Device.query.all()
    device_table = DevicesTable(devices)
    return render_template('admin_devices.html',
                           device_form=DeviceForm(),
                           device_table=device_table,
                           )


@admin.route('/schedules')
@login_required
def schedules_page():
    return render_template('admin_schedules.html',
                           schedule_form=ManualScheduleForm()
                           )


@admin.route('/login', methods=['POST'])
def login():
    admin_form = AdminLoginForm(request.form)
    if not admin_form.validate():
        redirect(url_for('views.login'))
        render_template('user.html', incorrect_login="Campos de login incorretos")

    admin = Admin.query.filter_by(username=admin_form.username.data).first()

    if admin:
        if admin.check_password(admin_form.password.data) or admin.is_authenticated():
            login_user(admin)
            return redirect(url_for('admin.users_edit_page'))
    flash('Administrador não encontrado', 'login_error')
    return redirect(url_for('views.login_page'))


# -----------------------------ACTIONS-------------------------

@admin.route("/addDevice", methods=['POST'])
@login_required
def add_device():
    form = request.form
    new_device = DeviceForm(form)
    if not new_device.errors:
        device_db = Device()
        device_db.name = new_device.device_name.data
        device_db.set_password(new_device.macAddress.data)
        try:
            db.session.add(device_db)
            db.session.commit()
        except:
            db.session.rollback()
        finally:
            return redirect(url_for('admin.devices_page'))
    return jsonify(error="Não foi possivel Adicionar o Dispositivo"), 400


@admin.route('/addUser', methods=['POST'])
@login_required
def addUser():
    user_form = UserForm(request.form)
    if user_form.validate():
        new_user = User()
        new_user.name = user_form.name.data
        new_user.card = user_form.card.data
        new_user.email = user_form.email.data
        new_user.bond = Bond.from_id(user_form.bond.data)
        try:
            db.session.add(new_user)
            db.session.commit()
        except Exception as e:
            print(str(e))
            db.session.rollback()
        finally:
            return "", 200
    return jsonify(error="não foi possivel adicionar o Usuários"), 400


@admin.route("/editUser", methods=['POST'])
@login_required
def editUser():
    json = request.get_json()
    id = json['id']
    name = json['name']
    card = json['card']
    email = json['email']
    bond = json['bond']

    user = User.query.filter_by(id=id).first()
    user.name = name
    user.card = card
    user.email = email
    user.bond = Bond.from_id(bond)
    db.session.commit()
    return jsonify()


@admin.route('/deleteUser', methods=['POST'])
@login_required
def deleteUser():
    id = 0
    try:
        id = request.args.get('id')
    except:
        return 400
    user = User.query.filter_by(id=id).first()
    if user:
        user.is_active = False
        db.session.commit()
    return redirect(url_for('admin.users_edit_page'))


@admin.route('/name/autocomplete', methods=['GET'])
@login_required
def name_autocomplete():
    name = request.args.get('name', '')
    result = []
    users = User.query.filter(User.name.like('%' + name + '%')).limit(5).all()
    for user in users:
        result.append(user.name)
    return jsonify(result)


@admin.route('/addSchedule', methods=['POST'])
@login_required
def add_schedule():
    form = ManualScheduleForm(request.form)
    if form.validate():
        user = User.query.filter_by(name=form.name.data).first()
        if user:
            enterTime = datetime.combine(form.date.data, form.time_in.data)
            exitTime = datetime.combine(form.date.data, form.time_out.data)
            scheduleEnter = Schedule()
            scheduleEnter.user_id = user.id
            scheduleEnter.datetime = enterTime
            scheduleEnter.device_id = 0

            scheduleExit = Schedule()
            scheduleExit.user_id = user.id
            scheduleExit.datetime = exitTime
            scheduleExit.device_id = 0

            db.session.add(scheduleEnter)
            db.session.add(scheduleExit)
            db.session.commit()
            return jsonify(), 200
        return jsonify(error="Usuarios não encontrado"), 400
    return jsonify(error="Campos Invalidos"), 400
