import datetime

from flask import Blueprint, request, jsonify

import database
from Blueprints.Wrarp import require_api_key
from Error import Error
from Model.Schedule import Schedule
from Model.User import User
from database import db

iot_device = Blueprint('iot_device', __name__)


@iot_device.route("/schedule", methods=['POST'])
@require_api_key
def check_shedule():
    if request.is_json:
        json = request.get_json()
        card = ''
        try:
            card = json['card']
            print('card: ' + card)
            database.fill_lasts_cards_list(card)

        except KeyError:
            return jsonify({'status': Error.ERROR_MISSING_VALUE.value})
        user = User.query.filter_by(card=card).first()
        if user is not None:
            try:
                schedule = Schedule()
                schedule.datetime = datetime.datetime.now()
                schedule.user_id = user.id
                schedule.device_id = 1
                db.session.add(schedule)
                db.session.commit()
            except Exception as e:
                print(str(e))
                db.session.rollback()
                return jsonify({'status': Error.ERROR_ON_DB.value})
            user.is_present = not user.is_present
            db.session.commit()
        else:
            return jsonify({'status': Error.ERROR_USER_NOT_FOUND.value})
    else:
        return jsonify({'status': Error.ERROR_NOT_A_JSON.value})
    return jsonify({'status': Error.NO_ERROR.value})
