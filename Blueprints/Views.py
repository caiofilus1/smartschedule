from datetime import datetime, timedelta, time
import time

from flask import Blueprint, render_template, request
from flask_login import login_required

import database
from Blueprints.Wrarp import last_day
from Model.Device import Device
from Model.Schedule import Schedule
from Model.User import User, Bond
from templates.Tables.DevicesTable import DevicesTable
from templates.Tables.OnlineUsersTable import OnlineUsersTable
from templates.Tables.UserTable import UserTable
from templates.Views.DeviceForm import DeviceForm
from templates.Views.LoginForm import LoginForm
from templates.Views.ManualScheduleForm import ManualScheduleForm
from templates.Views.SearchScheduleForm import SearchScheduleForm
from templates.Views.UserForm import UserForm

views = Blueprint('views', __name__)


@views.route('/', methods=['GET'])
def user_page():
    return render_template('user.html', search_form=SearchScheduleForm())


# @views.route('/admin/users')
# @login_required
# def admin_users_page():
#     devices = Device.query.all()
#     device_table = DevicesTable(devices)
#
#     users = User.query.order_by(User.name).all()
#     user_table = UserTable(users)
#
#     last_cards = database.last_cards
#
#     # schedule = Schedule.query.all()
#     # schedule_table = ScheduleTable(schedule)
#     return render_template('admin.html',
#                            user_form=UserForm(),
#                            user_table=user_table,
#                            last_cards=last_cards,
#                            bond_values=Bond.list()
#                            )


# @views.route('/admin/devices')
# @login_required
# def admin_devices_page():
#     devices = Device.query.all()
#     device_table = DevicesTable(devices)
#     return render_template('admin_devices.html',
#                            device_form=DeviceForm(),
#                            device_table=device_table,
#                            )


# @views.route('/admin/schedules')
# @login_required
# def admin_schedules_page():
#     return render_template('admin_schedules.html',
#                            schedule_form=ManualScheduleForm()
#                            )


@views.route('/login')
def login_page():
    return render_template('login.html', user_form=LoginForm())


@views.route('/dashboard')
def dashboard_page():
    now = datetime.now()
    sunday = last_day(now, 'sunday')
    users = User.query.order_by(User.name).all()
    for user in users:
        schedules = Schedule.query.filter(Schedule.datetime >= sunday).filter(
            Schedule.user_id == user.id).all()

        total_time = timedelta(days=0, minutes=0, seconds=0)
        id = 0
        try:
            while id < len(schedules):
                total_time += (schedules[id + 1].datetime - schedules[id].datetime)
                id += 2
        except IndexError:
            total_time += (now - schedules[id].datetime)
        setattr(user, 'total_time', total_time)

    user_table = OnlineUsersTable(users)
    return render_template('dashboardUsers.html', onlineTable=user_table)
