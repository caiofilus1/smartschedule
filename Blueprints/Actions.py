import datetime
import time
from flask import Blueprint, request, render_template, url_for, jsonify, flash
from flask_login import login_user, login_required
from werkzeug.utils import redirect

from Forms.AdminLoginForm import AdminLoginForm
from Forms.DeviceForm import DeviceForm
from Forms.SearchForm import SearchForm
from Model.Admin import Admin
from Model.Device import Device
from Model.Schedule import Schedule
from Model.User import User, UserOnlineSchema, Bond
from templates.Tables.DevicesTable import DevicesTable
from templates.Tables.ScheduleTable import ScheduleTable
from templates.Tables.SearchTable import SearchTable
from templates.Tables.UserTable import UserTable

from database import db, login_manager
from templates.Views.ManualScheduleForm import ManualScheduleForm
from templates.Views.SearchScheduleForm import SearchScheduleForm
from templates.Views.UserForm import UserForm

actions = Blueprint('actions', __name__)


@actions.errorhandler(401)
def not_auth(error):
    return redirect(url_for('views.login_page'))


@login_manager.unauthorized_handler
def unauth_handler():
    return redirect(url_for('views.login_page'))


@actions.route('/search', methods=['POST'])
def search_schedule():
    search = SearchScheduleForm(request.form)

    user = User.query.filter(User.name.like('%' + search.name.data + '%')).all()
    if len(user) == 1:
        user = user[0]

        schedules = Schedule.query.filter_by(user_id=user.id)
        schedules = schedules.filter(Schedule.datetime.between(search.datein.data, search.dateout.data))

        search_parms, total_time = SearchTable.get_table_elements(schedules)

        return jsonify(table=SearchTable(search_parms).__html__(), total_time=total_time)

    elif len(user) == 0:
        return jsonify(table='<p class="text-danger">usuário não encontrado</p>', total_time='')
    else:
        return jsonify(table='<p class="text-danger">mais de um usuario encontrado</p>', total_time='')


@actions.route('/search_fail/user_not_found')
def search_fail_not_found():
    return render_template('user.html', search_error="Usuario não encontrado")


@actions.route('/search_fail/empty_field')
def search_fail_empty_field():
    return render_template('user.html', search_error="Campo Vazio")


@actions.route('/search/<device_id>')
def search_in_device(device_id):
    pass


@actions.route('/dashboard/userList')
def userList():
    users = User.query.order_by(User.name).all()
    json_list = list()
    for user in users:
        user.bond = user.bond.value[0] if user.bond else 0
        json_list.append(UserOnlineSchema().dump(user).data)
    return jsonify(users=json_list)