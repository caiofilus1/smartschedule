from flask_wtf import FlaskForm
from wtforms import StringField, DateField
from wtforms.validators import DataRequired, InputRequired


class SearchForm(FlaskForm):
    search = StringField('search', validators=[DataRequired()])
    datein = DateField('datein')
    dateout = DateField('dateout')
