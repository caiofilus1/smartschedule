from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired,InputRequired


class DeviceForm(FlaskForm):
    device_name = StringField('device_name', validators=[DataRequired()])
    macAddress = StringField('macAddress', validators=[DataRequired()])

