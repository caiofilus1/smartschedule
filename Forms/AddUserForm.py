from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email, InputRequired


class AddUserForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    card = StringField('card', validators=[DataRequired()])
