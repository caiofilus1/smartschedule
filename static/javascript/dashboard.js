
var isOnlineVector = null;

function refreshTable(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "/dashboard/userList", true ); // false for synchronous request
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
                var users = JSON.parse(xmlHttp.responseText).users;
                var rows = document.getElementById("dashboardTable").getElementsByTagName('tbody')[0].rows
                //alert(String(rows[0].cells[0]))
                var i;
                for (i = 0; i < users.length; i++){
                    rows[i].cells[0].innerHTML = users[i].id;
                    rows[i].cells[1].innerHTML = users[i].name;
                    var svgCircle = rows[i].cells[2].getElementsByTagName('svg')[0].children[0];
                    if(users[i].is_present){
                            var color = getComputedStyle(document.documentElement).getPropertyValue('--green');
                            isOnlineVector[i] = true;
                    }else{
                            var color = getComputedStyle(document.documentElement).getPropertyValue('--red');
                            isOnlineVector[i] = false;
                    }
                    svgCircle.setAttribute("fill", color);
                }
        }
    }
    xmlHttp.send( null );
    var response = xmlHttp.responseText;
    return response;
};

function refreshHour(){
    var rows = document.getElementById("dashboardTable").getElementsByTagName('tbody')[0].rows;
    for (i = 0; i < rows.length; i++){
        if(isOnlineVector[i] == true){
                var lastCell = rows[i].cells[rows[i].cells.length - 1];
                var hour = Number(lastCell.innerHTML.split(':')[0]);
                var minute = Number(lastCell.innerHTML.split(':')[1]);
                minute = minute + 1;
                if(minute>= 60){
                    hour = hour + 1;
                    minute = 0;
                }
                lastCell.innerHTML = String(hour).padStart(2, "0") + ':' + String(minute).padStart(2, "0");
        }
    }
}

$(document).ready(function(){
    $window = $(window);
    //Captura cada elemento section com o data-type "background"
    $('section[data-type="background"]').each(function(){
        var $scroll = $(this);
            //Captura o evento scroll do navegador e modifica o backgroundPosition de acordo com seu deslocamento.
            $(window).scroll(function() {
                var yPos = -($window.scrollTop() / $scroll.data('speed'));
                var coords = '50% '+ yPos + 'px';
                $scroll.css({ backgroundPosition: coords });
            });
   });
});

window.onload = function(){
   refreshTable();
   setInterval(refreshTable, 10000);
   setInterval(refreshHour, 1000*60);
   isOnlineVector = new Array(document.getElementById("dashboardTable").getElementsByTagName('tbody')[0].rows.length);
   isOnlineVector.fill(false);
};
