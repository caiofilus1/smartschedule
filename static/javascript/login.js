
$(document).ready(function(){
    $window = $(window);
    //Captura cada elemento section com o data-type "background"
    $('section[data-type="background"]').each(function(){
        var $scroll = $(this);
            //Captura o evento scroll do navegador e modifica o backgroundPosition de acordo com seu deslocamento.
            $(window).scroll(function() {
                var yPos = -($window.scrollTop() / $scroll.data('speed'));
                var coords = '50% '+ yPos + 'px';
                $scroll.css({ backgroundPosition: coords });
            });
   });
});

$('#teste').plaxify({"xRange":210,"yRange":210});
$.plax.enable();

function formatDate(date) {
    var d = new Date(date),
        year = d.getFullYear(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function searchInWeek(){
    var d = new Date();
    d.setDate( d.getDate() - d.getDay());
    var form = document.getElementById("search-form");
    var datein = document.getElementById("datein");
    var dateout = document.getElementById("dateout");

    datein.value = formatDate(d);
    dateout.value = formatDate(new Date());
    submitSearchForm();
}
function searchInMonth(){
    var d = new Date();
    d = new Date(d.getFullYear(), d.getMonth(), 1);
    var form = document.getElementById("search-form");
    var datein = document.getElementById("datein");
    var dateout = document.getElementById("dateout");
    datein.value = formatDate(d);
    dateout.value = formatDate(new Date());
    submitSearchForm();
}

function searchInYear(){
    var d = new Date();
    d = new Date(d.getFullYear(), 0, 1);
    var form = document.getElementById("search-form");
    var datein = document.getElementById("datein");
    var dateout = document.getElementById("dateout");
    datein.value = formatDate(d);
    dateout.value = formatDate(new Date());
    submitSearchForm();
}

function submitSearchForm(){
    var form = $('#search-form');
    $("#search-table").empty();
    $.ajax({
    url: form.attr('action'),
    type: form.attr('method'),
    contentType: 'application/x-www-form-urlencoded',
    data: form.serialize(),
    success: function(data, textStatus, jqXHR) {
        $("#search-table").append(data['table']);
        var h2 = document.createElement("h2");
        h2.innerHTML = "Hora Trabalhadas " + data['total_time']['hour']+ ":" + data['total_time']['min'].pad(2);
        $("#search-table").append(h2);

    },
    error: function(jqXHR, textStatus, errorThrown) {
        var errorMessage = jqXHR.responseText;
        if (errorMessage.length > 0) {
            alert(errorMessage);
        }
    }
});

}

/*
function OnTableClick(){
    var row = document.getElementById('clickable-row');
    document.location = row.data-href;
    return false;
}*/