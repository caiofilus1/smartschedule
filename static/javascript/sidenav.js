
function closeNav() {
  document.getElementById("sidenav").style.width = "0";
}

function openNav() {
  document.getElementById("sidenav").style.width = "250px";
}


function init(){
    var nav = $('#sidenav'),
        menu = $('#sidenav_opener'),
        main = $('main'),
        open = false,
        hover = false;

        menu.on('click', function(){
            open = !open ? true : false;
            nav.toggleClass('menu-active');
            main.toggleClass('menu-active');
            nav.removeClass('menu-hover');
            main.removeClass('menu-hover');
        });
        menu.hover(function (){
            if(!open && !hover){
                nav.addClass('menu-hover');
                main.addClass('menu-hover');
                hover = !hover ? true : false;
            }

        }, function(){
            nav.removeClass('menu-hover');
            main.removeClass('menu-hover');
            hover = !hover ? true : false;
        });


};


$( document ).ready(function() {

    init();
});