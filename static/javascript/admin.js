var bondValues =
    [[1, 'Projeto de Extenção'],
    [2, 'Projeto de Voluntáriado Acadêmico'],
    [3, 'Projeto de Iniciação Ciêntifica'],
    [4, 'Estágio'],
    [5, 'Mestrado'],
    [6, 'Doutorado']
];

const COL_ID = 0;
const COL_NAME = 1;
const COL_CARD = 2;
const COL_EMAIL = 3;
const COL_BOND = 4;

function AddUser(){
    var form = document.findElementById('add_user_form')
    $('#add_user_form').submit(function(e){
        e.preventDefault();
        $.ajax({
            url:$('#add_user_form').attr('action'),
            type:$('#add_user_form').attr('method'),
            data:$('#add_user_form').serialize(),
            success:function(result){
                var modal = document.getElementById('myModal');
                var message = document.getElementById('dialog_message');
                modal.style.display = "block";
                message.innerHTML = 'Usuário Adicionado com Sucesso';
                setTimeout(closeDialog, 1000);
            },
            error:function(result){
                var modal = document.getElementById('myModal');
                var message = document.getElementById('dialog_message');
                modal.style.display = "block";
                message.innerHTML = 'Erro ao adicionar Usuario';
                setTimeout(closeDialog, 1000);
            }
        });
    });
}

function Edit(button){
    button.src = "/static/images/success.svg";
    button.setAttribute('onclick', "ConfirmEdit(this)");
//    var cancelEditButton = document.createElement("img");
//    button.src = "/static/images/cancel.svg";
//    button.parentElement.appendChild(cancelEditButton)

    var par = button.parentElement.parentElement;
    for(var i = 1; i < 4; i++){
        var d = document.createElement('input');
        d.value = par.childNodes[i].innerHTML;
        par.childNodes[i].innerHTML = "";
        par.childNodes[i].appendChild(d);
    }
    par.childNodes[COL_BOND].innerHTML = "";

    var select = document.createElement("SELECT");
    document.body.appendChild(select);
    for(var i = 0; i < bondValues.length; i++){
        var option = document.createElement("option");
        option.setAttribute("value", bondValues[i][0]);
        var text = document.createTextNode(bondValues[i][1]);
        option.appendChild(text);
        select.appendChild(option);
    }
    par.childNodes[COL_BOND].appendChild(select);
};

function ConfirmEdit(button){
    button.src = "/static/images/edit.svg";
    button.setAttribute('onclick', "Edit(this)");

    var par = button.parentElement.parentElement;
    var tdId = par.childNodes[COL_ID];
    var tdName = par.childNodes[COL_NAME];
    var tdCard = par.childNodes[COL_CARD];
    var tdEmail = par.childNodes[COL_EMAIL];
    var tdBond = par.childNodes[COL_BOND];
    var editButon = par.childNodes[par.childNodes.length - 1];

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "POST", "/admin/editUser", true ); // false for synchronous request
    xmlHttp.setRequestHeader('Content-Type', 'application/json')
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4){
            var modal = document.getElementById('myModal');
            var message = document.getElementById('dialog_message');
            modal.style.display = "block";
            if (xmlHttp.status == 200){
                message.innerHTML = 'Usuário Alterado com Sucesso';
            }else{
                message.innerHTML = 'Erro ao alterar o Usuario';
            }
            setTimeout(closeDialog, 1000);
        }
    }
    var json= {};
    json['id'] = parseInt(tdId.innerHTML);
    json['name'] = tdName.childNodes[0].value;
    json['card'] = tdCard.childNodes[0].value;
    json['email'] = tdEmail.childNodes[0].value;
    json['bond'] = parseInt(tdBond.childNodes[0].value);
    xmlHttp.send(JSON.stringify(json));

    for(var i = 1; i < 4; i++){
        par.childNodes[i].innerHTML = par.childNodes[i].childNodes[0].value;
    }
    tdBond.innerHTML = bondValues[tdBond.childNodes[0].selectedIndex][1];

    editButon.childNodes[0].style.visibility = "visible";
    editButon.childNodes[1].style.visibility = "hidden";
};


function deleteUser(button){
    var par = button.parentElement.parentElement;
    var tdId = par.childNodes[COL_ID];

    $.ajax({
            url:'/admin/',
            type:$('#add_user_form').attr('method'),
            data:$('#add_user_form').serialize(),
            success:function(result){
                var modal = document.getElementById('myModal');
                var message = document.getElementById('dialog_message');
                modal.style.display = "block";
                message.innerHTML = 'Usuário Excluido com Sucesso';
                setTimeout(closeDialog, 1000);
            },
            error:function(result){
                var modal = document.getElementById('myModal');
                var message = document.getElementById('dialog_message');
                modal.style.display = "block";
                message.innerHTML = 'Erro ao Excluir Usuario';
                setTimeout(closeDialog, 1000);
            }
        });
}

function closeDialog(){
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}


function autoComplete(input){
    var arg = "?name=" + String(input.value);
    closeAllLists(input.parentNode);
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "/admin/name/autocomplete" + arg, true ); // false for synchronous request
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4){
            var json = JSON.parse(this.responseText);
            a = document.createElement("div");
            a.setAttribute("class", "autocomplete_items");
            input.parentNode.appendChild(a);
            for(var i = 0; i < json.length; i++){
                b = document.createElement("div");
                b.innerHTML = "<strong>" + json[i].substr(0, input.value.length) + "</strong>";
                b.innerHTML += json[i].substr(input.value.length);
                b.innerHTML += "<input type='hidden' value='" + json[i] + "'>";
                b.addEventListener("click", function(e) {
                    input.value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    }
    xmlHttp.send(null);
}
function closeAllLists() {
    var x = document.getElementsByClassName("autocomplete_items");
    for (var i = 0; i < x.length; i++) {
        for(var j = 0; j < x[i].childNodes.length;  j++){
            x[i].removeChild(x[i].childNodes[j]);
        }
        x[i].remove();
    }
}

window.onload = function(){

    document.addEventListener("click", function () {
        closeAllLists();
    });

    $('#date').bootstrapMaterialDatePicker({
        time: false,
        format: 'YYYY-MM-DD',
        clearButton: false
    });
    $('#time_in').bootstrapMaterialDatePicker({
        date: false,
        shortTime: false,
        format: 'HH:mm',
        clearButton: false
    });
    $('#time_out').bootstrapMaterialDatePicker({
        date: false,
        shortTime: false,
        format: 'HH:mm',
        clearButton: false
    });
    openPage('NewUser',document.getElementById('defaultOpen'))
}