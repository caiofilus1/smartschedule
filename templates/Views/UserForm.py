import inspect

from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired

from Model.User import Bond


class UserForm(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()])
    email = EmailField('E-Mail', validators=[DataRequired()])
    card = StringField('Numero do Cartão', validators=[DataRequired()])
    bond = SelectField('Vinculo', coerce=int, choices=[i[0:2] for i in Bond.list()], validators=[DataRequired()])
