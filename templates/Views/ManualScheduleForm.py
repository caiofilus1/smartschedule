from flask_wtf import FlaskForm
from wtforms import StringField, DateField, TimeField
from wtforms.validators import DataRequired


class ManualScheduleForm(FlaskForm):
    name = StringField('Nome', id='name', validators=[DataRequired()])
    date = DateField('Data', id='date', validators=[DataRequired()])

    time_in = TimeField('Hora de entrada', id='time_in', validators=[DataRequired()])
    time_out = TimeField('Hora de saida', id='time_out', validators=[DataRequired()])
