from flask_wtf import FlaskForm
from wtforms import StringField, DateField
from wtforms.validators import DataRequired


class SearchScheduleForm(FlaskForm):
    name = StringField('Nome', id='name', validators=[DataRequired()])
    datein = DateField('Data Inicial', id='datein', validators=[DataRequired()])
    dateout = DateField('Data Final', id='dateout', validators=[DataRequired()])
