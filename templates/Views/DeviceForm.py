from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, HiddenField
from wtforms.validators import DataRequired


class DeviceForm(FlaskForm):
    device_name = StringField('device_name', validators=[DataRequired()])
    macAddress = StringField('macAddress', validators=[DataRequired()])
    next_url = HiddenField()
