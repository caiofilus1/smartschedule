import os

from flask import url_for
from flask_table import Table, Col, ButtonCol, LinkCol, BoolCol
from flask_table.html import element

from Model.User import Bond
from templates.Tables.BootstrapTable import BootstrapTable


class JSButtonCol(ButtonCol):

    def url(self, item):
        return item

    def td_contents(self, item, attr_list):
        button_attrs = dict(self.button_attrs)
        button_attrs['class'] = 'icon_button'
        button_attrs['onClick'] = 'Edit(this)'
        button_attrs['width'] = '30px'
        button_attrs['height'] = '30px'
        button_attrs['src'] = "/static/images/edit.svg"

        button = element(
            'img',
            attrs=button_attrs,
            content='',
            escape_content=False
        )
        button_attrs = button_attrs.copy()
        button_attrs['id'] = 'confirmButton'
        button_attrs['style'] = 'visibility : hidden; position: absolute; left: 0px; top: 12px;  ' \
                                'background: url(/static/images/success.svg);'
        button_attrs['onClick'] = 'ConfirmEdit(this)'
        hidenbutton = element(
            'button',
            attrs=button_attrs,
            content=self.text(item, attr_list),
        )
        return [button]

    def td(self, item, attr):
        content = self.td_contents(item, self.get_attr_list(attr))
        self.td_html_attrs['style'] = 'position: relative; '
        return element(
            'td',
            content=content,
            escape_content=False,
            attrs=self.td_html_attrs)


class DeleteRowCol(ButtonCol):

    def td_contents(self, item, attr_list):
        button_attrs = dict()
        button_attrs['id'] = "button_" + str(item.id)
        button_attrs['class'] = 'icon_button'
        button_attrs['onClick'] = 'deleteUser(this)'
        button_attrs['action'] = url_for('admin.deleteUser')
        button_attrs['method'] = "POST"
        button_attrs['width'] = '30px'
        button_attrs['height'] = '30px'
        button_attrs['src'] = url_for("static", filename='images/delete.svg')

        button = element(
            'img',
            attrs=button_attrs,
            content='',
            escape_content=False
        )
        return button


class ColBond(Col):
    def td_contents(self, item, attr_list):
        return item.bond.value[1] if item.bond else ''

    def td(self, item, attr):
        content = self.td_contents(item, self.get_attr_list(attr))
        # self.td_html_attrs['style'] = 'width: 200px;'
        return element(
            'td',
            content=content,
            escape_content=False,
            attrs=self.td_html_attrs)


class UserTable(BootstrapTable):
    name = Col('Nome')
    card = Col('Cartão')
    email = Col('Email')
    bond = ColBond('Vinculo')

    delete_button = DeleteRowCol('',
                                 '',
                                 )
    edit_button = JSButtonCol('',
                              ''
                              )
