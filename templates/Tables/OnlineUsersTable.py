from datetime import date

from flask_table import Table, Col
from flask_table.html import element
from sqlalchemy import func

from Model.Schedule import Schedule
from Model.User import User


class ImageCol(Col):
    buttonsId = 0

    def url(self, item):
        return item

    def td_contents(self, item, attr_list):

        attrs = dict(width="24", height="24")
        if item.is_present:
            content = dict(cx="12", cy="12", r="10", stroke="black", stroke_width="4", fill="#008853")
        else:
            content = dict(cx="12", cy="12", r="10", stroke="black", stroke_width="4", fill="#881700")
        # Schedule.query.filter_by(func.date(Schedule.datetime) == date.today()).all()
        button = element(
            'svg',
            attrs=attrs,
            content=element('circle', attrs=content),
            escape_content=False
        )
        return button

    def get_tr_attrs(self, item):
        return {'src': "{{url_for('static', filename='images/delete.svg')}}"}


class CompleteHoursCol(Col):

    def td(self, item, attr):
        hours = (item.total_time.days * 24) + item.total_time.seconds // 3600
        if item.bond and hours >= item.bond.value[2]:
            attr = dict(style="color: #008853")
        else:
            attr = dict(style="color: #881700")
        return element(
            'td',
            content=str(hours) + ':' + str((item.total_time.seconds // 60) % 60),
            escape_content=False,
            attrs=attr)


class OnlineUsersTable(Table):
    id = Col('Id', show=True)
    name = Col('Nome')
    status = ImageCol('Status')
    total_time = CompleteHoursCol('Horas na Semana')

    table_id = 'dashboardTable'
