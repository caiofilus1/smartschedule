from flask_table import Table, Col, DateCol, DatetimeCol

from templates.Tables.BootstrapTable import BootstrapTable

_enter_act = ['Entrada', 'Saida']


class MyDatetimeCol(DatetimeCol):
    def td_format(self, content):
        if content:
            return content.strftime('%d/%m/%Y %H:%M')
        else:
            return ''


class SearchTable(BootstrapTable):
    id = Col('id', show=False)
    datetime = MyDatetimeCol('Horario', datetime_format="short", td_html_attrs={'scope': 'col'})
    action = Col('Acão')

    @staticmethod
    def get_table_elements(query):
        '''
        :param query: resultado do query do banco de dados
        :return: Tabela formada, Horas totais, minutos
        '''
        result = list()
        deltaTime = 0
        sum = 0
        enter_flag = False
        for element in query:
            if enter_flag:
                result.append(dict(id=0, datetime=element.datetime, action=_enter_act[1]))
                deltaTime += (element.datetime - sum).total_seconds()
            else:
                result.append(dict(id=0, datetime=element.datetime, action=_enter_act[0]))
                sum = element.datetime
            enter_flag = not enter_flag

        totalHour = dict(hour=int(deltaTime / 3600), min=int(divmod(deltaTime, 3600)[1] / 60))
        return result, totalHour
