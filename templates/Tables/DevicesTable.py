from flask_table import Table, Col


class DevicesTable(Table):
    id = Col('Id', show=True)
    name = Col('Nome')
    api_key = Col('Sha256')
