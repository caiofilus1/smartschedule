from flask_table import Table, Col
from flask_table.html import element


class BootstrapTable(Table):
    id = Col('Id', show=True, td_html_attrs={'scope': 'col'})

    classes = 'table'

    def get_html_attrs(self):
        attrs = dict(self.html_attrs) if self.html_attrs else {}
        if self.table_id:
            attrs['id'] = self.table_id
        if self.classes:
            attrs['class'] = self.classes
        if self.border:
            attrs['border'] = 1
        return attrs

    def th(self, col_key, col):
        col.th_html_attrs['scope'] = 'col'
        return element(
            'th',
            content=self.th_contents(col_key, col),
            escape_content=False,
            attrs=col.th_html_attrs,
        )
