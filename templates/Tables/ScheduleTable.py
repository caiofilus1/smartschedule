from flask_table import Table, Col


class ScheduleTable(Table):

    id = Col('Id', show=True)
    datetime = Col('Horario')
    user_id = Col('Id do Usuario')