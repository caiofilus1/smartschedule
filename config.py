import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # ...
    '''SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI') or \
                              'sqlite:///' + os.path.join(basedir, 'easy_schedule.db')
    '''
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI') or \
                              'sqlite:///' + os.path.join(basedir, 'easy_schedule.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'development key'
    USERNAME = 'cptufpr@gmail.com'
    PASSWORD = 'Microalgas'
