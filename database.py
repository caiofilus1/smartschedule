from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from config import Config
from flask_login import LoginManager

db = None
login_manager = None
last_cards = ['']*5
last_card_id = 0
current_device = None
marsh_mallow = None

def init_db(app):
    global db
    global login_manager
    global marsh_mallow
    app.config.from_object(Config)
    db = SQLAlchemy(app)
    marsh_mallow = Marshmallow(app)
    login_manager = LoginManager()
    login_manager.init_app(app)

    from Model import User, Schedule, Device, Admin
    db.create_all()


def fill_lasts_cards_list(card):
    global last_card_id
    if last_card_id >= 5:
        last_card_id = 0
    last_cards[last_card_id] = card + ", "
    last_card_id += 1
