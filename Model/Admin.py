from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from database import login_manager
from database import db


class Admin(db.Model, UserMixin):
    __tablename__ = "admin"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(128), index=True)

    def __repr__(self):
        return "<User %r>" % self.username

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @staticmethod
    @login_manager.user_loader
    def load_user(user_id):
        return Admin.query.get(user_id)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id
