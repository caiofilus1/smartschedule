
from werkzeug.security import generate_password_hash, check_password_hash

from database import db


class Device(db.Model):
    __tablename__ = "device"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False, unique=True)
    api_key = db.Column(db.String(128), nullable=False)

    def set_password(self, api_key):
        self.api_key = generate_password_hash(api_key)

    def check_password(self, api_key):
        return check_password_hash(self.api_key, api_key)
