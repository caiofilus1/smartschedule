import enum

from sqlalchemy.types import *
from database import db, marsh_mallow


class Bond(enum.Enum):
    EXTENSION = (1, 'Projeto de Extenção', 12)
    PVA = (2, 'Projeto de Voluntáriado Acadêmico', 12)
    INI_CINETIFIC = (3, 'Projeto de Iniciação Ciêntifica', 20)
    INTERNSHIP = (4, 'Estágio', 30)
    MASTER = (5, 'Mestrado', 40)
    DOCTORATE = (6, 'Doutorado', 40)

    @classmethod
    def from_id(cls, id):
        for item in cls:
            print(item.value[0])
            if item.value[0] == id:
                return item
        raise ValueError("%r is not a valid %s id" % (id, cls.__name__))

    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    card = db.Column(db.String(64), nullable=False)
    email = db.Column(db.String(64), default=None)
    bond = db.Column(Enum(Bond), default=Bond.EXTENSION)
    is_present = db.Column(db.Boolean, default=False)
    is_active = db.Column(db.Boolean, default=True)


class UserOnlineSchema(marsh_mallow.ModelSchema):
    strict = True

    class Meta:
        model = User
