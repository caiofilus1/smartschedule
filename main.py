from functools import wraps

import datetime
import _thread

from flask import Flask, request, jsonify, render_template, redirect, url_for

import jwt
import os
import time

from sqlalchemy import func

import database

app = Flask(__name__)
with app.app_context():
    database.init_db(app)

from Model.User import User
from Model.Schedule import Schedule
from Model.Device import Device
from Model.Admin import Admin

from database import db, last_cards, last_card_id

from Blueprints.Actions import actions
from Blueprints.Views import views
from Blueprints.IotDevice import iot_device
from Blueprints.Admin.Admin import admin

from datetime import date
import schedule

app.register_blueprint(actions)
app.register_blueprint(views)
app.register_blueprint(iot_device)
app.register_blueprint(admin)

devices_logged = list()


# def require_api_key(f):
#     @wraps(f)
#     def decorator(*args, **kwargs):
#         if request.is_json:
#             json = request.get_json()
#             api_key = ''
#             try:
#                 api_key = json['api_key']
#             except KeyError:
#                 return jsonify({'status': Error.ERROR_MISSING_VALUE.value})
#
#             device = Device.query.filter_by(api_key=api_key).first()
#             '''for device_logged in devices_logged:
#                 if device_logged['device'].id == device.id:
#                     if device_logged['ip'] != request.remote_addr:
#                         device_logged['ip'] = request.remote_addr
#                         break
#
#             devices_logged.append({'device': device, 'ip': request.remote_addr})
# '''''
#             if device is not None:
#                 global current_device
#                 current_device = device
#                 return f(*args, **kwargs)
#             else:
#                 return jsonify({'status': Error.ERROR_INVALID_TOKEN.value})
#         else:
#             return jsonify({'status': Error.ERROR_NOT_A_JSON.value})
#
#     return decorator


@app.route("/addAdmin", methods=['GET'])
def admin():
    user = Admin()
    user.username = "teste"
    user.set_password("123456")
    db.session.add(user)
    db.session.commit()
    return "passei aqui cara"


@app.route("/user", methods=['GET'])
def teste():
    user = User()
    user.card = 'B5 C9 99 43'
    user.name = 'Peter o mimico'
    db.session.add(user)
    db.session.commit()
    return "passei aqui cara"


@app.route("/device", methods=['GET'])
def device():
    device = Device()
    device.name = "ESP8266 startup"
    device.set_password('5C:CF:7F:ED:ED:8E')
    db.session.add(device)
    db.session.commit()
    return "passei aqui cara"


@app.route("/devicesd", methods=['GET'])
def add_colunm():
    pass


# @app.route("/testSchedule", methods=['GET'])
# def testSchedule():
#     return check_shedule(dict(card='95 B4 E5 4C'))

    # @app.route("/schedule", methods=['POST'])
    # @require_api_key
    # def check_shedule():
    #     if request.is_json:
    #         json = request.get_json()
    #         card = ''
    #         try:
    #             card = json['card']
    #             print('card: ' + card)
    #             database.fill_lasts_cards_list(card)
    #
    #         except KeyError:
    #             return jsonify({'status': Error.ERROR_MISSING_VALUE.value})
    #         user = User.query.filter_by(card=card).first()
    #         if user is not None:
    #             try:
    #                 schedule = Schedule()
    #                 schedule.datetime = datetime.datetime.now()
    #                 schedule.user_id = user.id
    #                 schedule.device_id = current_device.id
    #                 db.session.add(schedule)
    #                 db.session.commit()
    #             except Exception as e:
    #                 print(str(e))
    #                 db.session.rollback()
    #                 return jsonify({'status': Error.ERROR_ON_DB.value})
    #         else:
    #             return jsonify({'status': Error.ERROR_USER_NOT_FOUND.value})
    #     else:
    #         return jsonify({'status': Error.ERROR_NOT_A_JSON.value})
    #     return jsonify({'status': Error.NO_ERROR.value})


def print_date_time():
    today_shedules = Schedule.query.filter(func.date(Schedule.datetime) == date.today()).all()
    id_list = list()
    for shedule in today_shedules:
        if shedule.user_id not in id_list:
            id_list.append(shedule.user_id)
        else:
            id_list.remove(shedule.user_id)
    not_exit_shedules = list()
    for shedule in today_shedules:
        for id in id_list:
            if id == shedule.user_id:
                not_exit_shedules.append(shedule)

    for shedule in not_exit_shedules:
        user_id = shedule.user_id
        try:
            db.session.delete(shedule)
            db.session.commit()
        except Exception as e:
            print(str(e))
        user = User.query.filter_by(id=user_id).first()
        if user:
            user.is_present = False
            db.session.commit()
    return


def schedule_handler():
    while 1:
        try:
            time.sleep(60 * 30)
            schedule.run_pending()
        except Exception as e:
            print(str(e))


schedule.every().day.at("23:30").do(print_date_time)
_thread.start_new_thread(schedule_handler, ())

# app.secret_key = os.urandom(12)
app.secret_key = 'glauber'
# app.run(host='127.0.0.1', port=5001, debug=True)
if __name__ == '__main__':
    app.config['DEBUG'] = True
    app.config['SECRET_KEY'] = 'glauber' if app.config['DEBUG'] else os.urandom(12)
    app.run(host='0.0.0.0')
