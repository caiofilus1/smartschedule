
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from config import Config
from main import app
from database import db

app.config.from_object(Config)

# Import database models with app context
with app.app_context():
  from Model import *

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()